﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderDishes;
using OrderDishes.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace OrderDishes.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class DishController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public DishController(ApplicationDbContext context)
        {
            this.context = context;
        }

        // GET: api/Dish
        [HttpGet]
        public string GetDishes()
        {
            var dishes = context.Dishes.ToList();
            return JsonConvert.SerializeObject(dishes, Formatting.Indented);
        }

        // GET: api/Dish/FindDishes?name=Манты
        [HttpGet("FindDishes")]
        public string FindDishes(string name)
        {
            //var dishes = context.Dishes.ToList();
            //var dishes = context.Dishes.Find(id);Where(t => ...)
            var dishes = context.Dishes.Where(d => d.Name == name);
            return JsonConvert.SerializeObject(dishes, Formatting.Indented);
        }


        // POST: api/Dish
        [HttpPost]
        public string PostDishes([FromForm] Dish dish)
        {
            context.Dishes.Add(dish);
            context.SaveChanges();

            return GetDishes();
        }

        // PUT: api/Dish/5
        [HttpPut("{id}")]
        public void PutDish(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Dish/5
        [HttpDelete("{id}")]
        public void DeleteDish(int id)
        {
        }

       private string GetHtmlPage(string content)
        {
            string head = "<!DOCTYPE HTML PUBLIC \" -//IETF//DTD HTML 2.0//EN\">";
            return head + $"<html><head></head><body>{content}</body></html>";
        }
    }
}
