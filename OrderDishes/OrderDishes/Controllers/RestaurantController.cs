﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderDishes.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OrderDishes;

namespace OrderDishes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public RestaurantController(ApplicationDbContext context)
        {
            this.context = context;
        }

        // GET: api/Restaurant
        [HttpGet]
        public string GetRestaurants()
        {
            var restaurants = context.Restaurants.ToList();
            return JsonConvert.SerializeObject(restaurants, Formatting.Indented);
        }

        // GET: api/Restaurant/5
        [HttpGet("{id}", Name = "GetRestaurants")]
        public string GetRestaurants(int id)
        {
            return "value";
        }

        // POST: api/Restaurant
        [HttpPost]
        public string PostRestaurants([FromForm] Restaurant restaurant)
        {
            context.Restaurants.Add(restaurant);
            context.SaveChanges();

            return GetRestaurants();
        }

        // PUT: api/Restaurant/5
        [HttpPut("{id}")]
        public void PutRestaurant(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Restaurant/5
        [HttpDelete("{id}")]
        public void DeleteRestaurant(int id)
        {
        }

        private string GetHtmlPage(string content)
        {
            string head = "<!DOCTYPE HTML PUBLIC \" -//IETF//DTD HTML 2.0//EN\">";
            return head + $"<html><head></head><body>{content}</body></html>";
        }
    }
}
