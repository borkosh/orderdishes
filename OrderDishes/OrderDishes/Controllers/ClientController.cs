﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OrderClients;
using OrderDishes;
using OrderDishes.Models;

namespace OrderClients.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public ClientController(ApplicationDbContext context)
        {
            this.context = context;
        }

        // GET: api/Client
        [HttpGet]
        public string GetClients()
        {
            var clients = context.Clients.ToList();
            return JsonConvert.SerializeObject(clients, Formatting.Indented);
        }

        // GET: api/Client/FindClients?name=Иван
        [HttpGet("FindClients")]
        public string FindClients(string name)
        {
            var clients = context.Clients.Where(d => d.Name == name);
            return JsonConvert.SerializeObject(clients, Formatting.Indented);
        }


        // POST: api/Client
        [HttpPost]
        public string PostClients([FromForm] Client Client)
        {
            context.Clients.Add(Client);
            context.SaveChanges();

            return GetClients();
        }

        // PUT: api/Client/5
        [HttpPut("{id}")]
        public void PutClient(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Client/5
        [HttpDelete("{id}")]
        public void DeleteClient(int id)
        {
        }

       private string GetHtmlPage(string content)
        {
            string head = "<!DOCTYPE HTML PUBLIC \" -//IETF//DTD HTML 2.0//EN\">";
            return head + $"<html><head></head><body>{content}</body></html>";
        }
    }
}
