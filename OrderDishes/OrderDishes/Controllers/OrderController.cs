﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OrderOrders;
using OrderDishes.Models;
using OrderDishes;
using Microsoft.EntityFrameworkCore;

namespace OrderOrders.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public OrderController(ApplicationDbContext context)
        {
            this.context = context;
        }

        // GET: api/Order
        [HttpGet]
        public string GetOrders()
        {
            var orders = context.Orders.ToList();
            return JsonConvert.SerializeObject(orders, Formatting.Indented);
        }

        // GET: api/Order/FindOrders?ClientId=1&name=Манты
        [HttpGet("FindOrders")]
        public string FindOrders(int ClientId, string name)
        {

            var orders = context.Orders.Include(o => o.Dish).Where(o => o.ClientId == ClientId && o.Dish.Name.Contains(name));

            JsonSerializerSettings jss = new JsonSerializerSettings();
            jss.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            return JsonConvert.SerializeObject(orders, jss);
        }


        // POST: api/Order
        [HttpPost]
        public string PostOrders([FromForm] Order order)
        {
            context.Orders.Add(order);
            context.SaveChanges();

            return GetOrders();
        }

        // PUT: api/Order/5
        [HttpPut("{id}")]
        public void PutOrder(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Order/5
        [HttpDelete("{id}")]
        public void DeleteOrder(int id)
        {
        }

       private string GetHtmlPage(string content)
        {
            string head = "<!DOCTYPE HTML PUBLIC \" -//IETF//DTD HTML 2.0//EN\">";
            return head + $"<html><head></head><body>{content}</body></html>";
        }
    }
}
