﻿using Microsoft.EntityFrameworkCore;
using OrderDishes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDishes
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }        
        

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //fluent api

            // Описание связей ресторана и блюда
            modelBuilder.Entity<Dish>()
                .HasOne(p => p.Restaurant)
                .WithMany(p => p.Dishes)
                .HasForeignKey(p => p.RestaurantId);

            modelBuilder.Entity<Restaurant>()
                .HasMany(c => c.Dishes)
                .WithOne(c => c.Restaurant)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Restrict);

            // Описание связей Блюда и заказа
            modelBuilder.Entity<Order>()
                .HasOne(o => o.Dish)
                .WithMany(o => o.Orders)
                .HasForeignKey(o => o.DishId);

            modelBuilder.Entity<Dish>()
                .HasMany(p => p.Orders)
                .WithOne(p => p.Dish)
                .HasPrincipalKey(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);

            // Описание связей Клиента и заказа
            modelBuilder.Entity<Order>()
                .HasOne(o => o.Client)
                .WithMany(o => o.Orders)
                .HasForeignKey(o => o.ClientId);

            modelBuilder.Entity<Client>()
                .HasMany(p => p.Orders)
                .WithOne(p => p.Client)
                .HasPrincipalKey(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
