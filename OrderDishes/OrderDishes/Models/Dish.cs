﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDishes.Models
{
    public class Dish
    {
        public int Id { get; set; }

        [FromForm(Name = "name")]
        public string Name { get; set; }

        [FromForm(Name = "price")]
        public double Price { get; set; }

        [FromForm(Name = "description")]
        public string Description { get; set; }

        [ForeignKey("Restaurant")]
        [FromForm(Name = "restaurantId")]
        public int RestaurantId { get; set; }

        public Restaurant Restaurant { get; set; }

        public IEnumerable<Order> Orders { get; set; }
    }
}
