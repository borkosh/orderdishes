﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDishes.Models
{
    public class Restaurant
    {
        public int Id { get; set; }

        [FromForm(Name = "name")]
        public string Name { get; set; }

        [FromForm(Name = "description")]
        public string Description { get; set; }

        public IEnumerable<Dish> Dishes { get; set; }
    }
}
