﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDishes.Models
{
    public class Client
    {
        public int Id { get; set; }

        [FromForm(Name = "name")]
        public string Name { get; set; }

        [FromForm(Name = "contacts")]
        public string Contacts { get; set; }

        public IEnumerable<Order> Orders { get; set; }

    }
}
