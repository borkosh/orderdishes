﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDishes.Models
{
    public class Order
    {
        public int Id { get; set; }

        [FromForm(Name = "orderdate")]
        public DateTime OrderDate { get; set; }

        [FromForm(Name = "clientid")]
        public int ClientId { get; set; }
        public Client Client { get; set; }

        [FromForm(Name = "dishid")]
        public int DishId { get; set; }        
        public Dish Dish { get; set; }
    }
}
